#include "MockDisplayAdapter.h"
#include "game.h"
#include "mockArduino.h"
#include <gtest/gtest.h>

class gametest : public ::testing::Test {
protected:
  MockDisplayAdapter m;

  void SetUp() override {
    mock_init();
    matrix = &m;
  }
};

TEST_F(gametest, setup_callsRandomSeedWithAnalogReadFromA7) {
  analogRead_will_return(27);

  gameSetup();

  EXPECT_EQ(27, randomSeed_called_with());
  EXPECT_EQ(A7, analogRead_called_with());
}

TEST_F(gametest, setup_randomlyPopulatesBoard) {
  random_will_return(1);

  gameSetup();

  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      ASSERT_TRUE(currentGeneration->get(x, y));
    }
  }
}

TEST_F(gametest, setup_marksCellsDeadWhenRandomReturnsZero) {
  random_will_return(0);

  gameSetup();

  ASSERT_EQ(2, random_called_with());

  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      ASSERT_FALSE(currentGeneration->get(x, y));
    }
  }
}

TEST_F(gametest, setup_beginsmatrixdisplay) {
  random_will_return(1);

  gameSetup();

  EXPECT_TRUE(m.begin_called());
}

TEST_F(gametest, setup_displaysStateOfBoard) {
  gameSetup();

  EXPECT_EQ(XMAX * YMAX, m.drawPixel_call_count());
}

TEST_F(gametest, setup_displaysLiveCellsAsLiveColor) {
  m.color_will_return(77);
  random_will_return(1);

  gameSetup();

  EXPECT_EQ(77, m.drawPixel_color());
}

TEST_F(gametest, setup_displaysDeadCellsAsDeadColor) {
  m.color_will_return(77);
  random_will_return(0);

  gameSetup();

  EXPECT_EQ(0, m.drawPixel_color());
}

TEST_F(gametest, loop_appliesRulesToBoard) {

  // Dead cell with three neighbors, should be alive
  currentGeneration->set(0, 1, true);
  currentGeneration->set(1, 0, true);
  currentGeneration->set(1, 1, true);

  // Live cell with four neighbors, should die
  currentGeneration->set(5, 4, true);
  currentGeneration->set(4, 5, true);
  currentGeneration->set(5, 5, true);
  currentGeneration->set(6, 5, true);
  currentGeneration->set(5, 6, true);

  gameLoop();

  EXPECT_TRUE(currentGeneration->get(0, 0));
  EXPECT_FALSE(currentGeneration->get(5, 5));
}

TEST_F(gametest, loop_makesFutureGenerationIntoCurrentGeneration) {
  Board *oldfuture = futureGeneration;
  Board *oldcurrent = currentGeneration;

  gameLoop();

  EXPECT_NE(futureGeneration, currentGeneration);
  EXPECT_EQ(oldcurrent, futureGeneration);
  EXPECT_EQ(oldfuture, currentGeneration);
}

TEST_F(gametest, loop_displaysBoard) {
  gameLoop();

  EXPECT_EQ(XMAX * YMAX, m.drawPixel_call_count());
}

TEST_F(gametest, loop_delaysFor500ms) {
  gameLoop();

  EXPECT_EQ(500, delay_called_with());
}