//
// Created by clay on 12/27/2019.
//

#include "rules.h"
#include <gtest/gtest.h>

TEST(rules, LiveCellWithThreeNeighborsLives) { EXPECT_TRUE(isAlive(3, true)); }

TEST(rules, LiveCellWithTwoNeighborsLives) { EXPECT_TRUE(isAlive(2, true)); }

TEST(rules, DeadCellWithTwoNeighborsStaysDead) {
  EXPECT_FALSE(isAlive(2, false));
}

TEST(rules, DeadCellWithThreeNeighborsLives) { EXPECT_TRUE(isAlive(3, false)); }

TEST(rules, LonelyCellsDie) {
  EXPECT_FALSE(isAlive(1, true));
  EXPECT_FALSE(isAlive(0, true));
}

TEST(rules, OvercrowdedCellsDie) {
  EXPECT_FALSE(isAlive(4, true));
  EXPECT_FALSE(isAlive(5, true));
  EXPECT_FALSE(isAlive(6, true));
  EXPECT_FALSE(isAlive(7, true));
  EXPECT_FALSE(isAlive(8, true));
}