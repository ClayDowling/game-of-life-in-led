#include "MockDisplayAdapter.h"

MockDisplayAdapter::MockDisplayAdapter() {
  begin_call = false;
  drawPixel_num_calls = 0;
  drawPixel_color_call = 0;
  color_return_value = 0;
}

void MockDisplayAdapter::begin(void) { begin_call = true; }
bool MockDisplayAdapter::begin_called() { return begin_call; }

void MockDisplayAdapter::drawPixel(int16_t x, int16_t y, int16_t c) {
  drawPixel_num_calls++;
  drawPixel_color_call = c;
}

int16_t MockDisplayAdapter::drawPixel_color() { return drawPixel_color_call; }

int MockDisplayAdapter::drawPixel_call_count() { return drawPixel_num_calls; }

uint16_t MockDisplayAdapter::color(uint8_t r, uint8_t g, uint8_t b) {
  return color_return_value;
}

void MockDisplayAdapter::color_will_return(uint16_t value) {
  color_return_value = value;
}