//
// Created by clay on 12/27/2019.
//

#include "board.h"
#include <gtest/gtest.h>

class BoardTest : public testing::Test {
protected:
  Board board;
};

TEST_F(BoardTest, GetFollowingSetTrue_ReturnsTrue) {
  board.set(7, 7, true);
  EXPECT_TRUE(board.get(7, 7));
}

TEST_F(BoardTest, GetWithoutSetTrue_ReturnsFalse) {
  EXPECT_FALSE(board.get(7, 7));
}

TEST_F(BoardTest, Get_SetTrueFollowedBySetFalse_ReturnsFalse) {
  board.set(4, 4, true);
  EXPECT_TRUE(board.get(4, 4));
  board.set(4, 4, false);
  EXPECT_FALSE(board.get(4, 4));
}

TEST_F(BoardTest, Neighbors_WithNoLiveNeighbors_Returns0) {
  EXPECT_EQ(0, board.neighbors(4, 4));
}

TEST_F(BoardTest, Neighbors_WithOneLiveNeighbor_Returns1) {
  board.set(3, 3, true);
  EXPECT_EQ(1, board.neighbors(4, 4));
}

TEST_F(BoardTest, Neighbors_WithAllNeighborsFilledOnLiveCell_Returns8) {
  for (uint8_t x = 3; x < 6; ++x) {
    for (uint8_t y = 3; y < 6; ++y) {
      board.set(x, y, true);
    }
  }
  EXPECT_EQ(8, board.neighbors(4, 4));
}

TEST_F(BoardTest, Neighbors_AtCornerOnFullBoard_Returns3) {
  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      board.set(x, y, true);
    }
  }
  ASSERT_TRUE(board.get(0, 1));
  ASSERT_TRUE(board.get(1, 0));
  ASSERT_TRUE(board.get(1, 1));
  EXPECT_EQ(3, board.neighbors(0, 0));
  //   EXPECT_EQ(3, board.neighbors(0, YMAX - 1));
  //   EXPECT_EQ(3, board.neighbors(XMAX - 1, 0));
  //   EXPECT_EQ(3, board.neighbors(XMAX - 1, YMAX - 1));
}