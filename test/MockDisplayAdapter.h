#include "DisplayAdapter.h"

class MockDisplayAdapter : public DisplayAdapter {
public:
  MockDisplayAdapter();

  virtual void begin(void) override;
  bool begin_called();

  virtual void drawPixel(int16_t x, int16_t y, int16_t c) override;
  int drawPixel_call_count();
  int16_t drawPixel_color();

  virtual uint16_t color(uint8_t r, uint8_t g, uint8_t b) override;
  void color_will_return(uint16_t value);

private:
  bool begin_call;
  int drawPixel_num_calls;
  int16_t drawPixel_color_call;
  uint16_t color_return_value;
};