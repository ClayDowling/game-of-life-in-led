#ifndef _MOCKARDUINO_H_
#define _MOCKARDUINO_H_

#include "Arduino.h"

void mock_init(void);

unsigned long randomSeed_called_with();

long random_called_with();
void random_will_return(long);
unsigned long random_call_count();

uint8_t analogRead_called_with();
void analogRead_will_return(int);

unsigned long delay_called_with();

#endif