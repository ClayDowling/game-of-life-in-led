#include "mockArduino.h"
#include "Arduino.h"

unsigned long randomSeed_call = 0;
long random_call = 0;
unsigned long random_num_calls = 0;
long random_return_value = 0;
uint8_t analogRead_call = 0;
int analogRead_return_value = 0;
unsigned long delay_call = 0;

void mock_init(void) {
  random_num_calls = 0;
  randomSeed_call = 0;
  random_call = 0;
  random_return_value = 0;
  analogRead_call = 0;
  analogRead_return_value = 0;
}

void pinMode(uint8_t pin, uint8_t mode) {
  // Do Nothing
}
void digitalWrite(uint8_t pin, uint8_t value) {
  // Do Nothing
}
int digitalRead(uint8_t pin) { return 0; }

void randomSeed(unsigned long seed) { randomSeed_call = seed; }
unsigned long randomSeed_called_with() { return randomSeed_call; }

long random(long upperbound) {
  random_call = upperbound;
  random_num_calls++;
  return random_return_value;
}
long random_called_with() { return random_call; }
void random_will_return(long returnvalue) { random_return_value = returnvalue; }
unsigned long random_call_count() { return random_num_calls; }

int analogRead(uint8_t pin) {
  analogRead_call = pin;
  return analogRead_return_value;
}
uint8_t analogRead_called_with() { return analogRead_call; }
void analogRead_will_return(int returnvalue) {
  analogRead_return_value = returnvalue;
}

void delay(unsigned long ms) { delay_call = ms; }

unsigned long delay_called_with() { return delay_call; }
