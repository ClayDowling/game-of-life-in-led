#include "LedDisplayAdapter.h"
#include "game.h"

// Configuration for the expected wiring of a 64x32 panel on an Arduino Mega
// 2560
#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE 9
#define LAT 10
#define A A0
#define B A1
#define C A2
#define D A3

LedDisplayAdapter panel(A, B, C, D, CLK, LAT, OE, false, 64);

void setup() {
  matrix = &panel;
  gameSetup();
}

void loop() { gameLoop(); }