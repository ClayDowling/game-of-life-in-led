#include "DisplayAdapter.h"
#include "board.h"
#include "rules.h"
#include <Arduino.h>

#define CLK 11 // USE THIS ON ARDUINO MEGA
#define OE 9
#define LAT 10
#define A A0
#define B A1
#define C A2
#define D A3

// RGBmatrixPanel matrix(A, B, C, D, CLK, LAT, OE, false, 64);

Board b1;
Board b2;

Board *currentGeneration = &b1;
Board *futureGeneration = &b2;
Board *tmpGeneration;

DisplayAdapter *matrix = NULL;

uint16_t live_color;
uint16_t dead_color = 0;

void gameDisplay() {
  uint16_t c;
  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      c = currentGeneration->get(x, y) ? live_color : dead_color;
      matrix->drawPixel(x, y, c);
    }
  }
}

void gameSetup() {
  // Initialize random number generator
  randomSeed(analogRead(A7));
  live_color = matrix->color(127, 127, 127);

  // Initialize board
  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      currentGeneration->set(x, y, random(2));
    }
  }

  // Display initial state
  matrix->begin();
  gameDisplay();
}

void gameLoop() {

  // Apply rules to board
  for (uint8_t x = 0; x < XMAX; ++x) {
    for (uint8_t y = 0; y < YMAX; ++y) {
      futureGeneration->set(x, y,
                            isAlive(currentGeneration->neighbors(x, y),
                                    currentGeneration->get(x, y)));
    }
  }
  tmpGeneration = futureGeneration;
  futureGeneration = currentGeneration;
  currentGeneration = tmpGeneration;

  // Display board
  gameDisplay();

  // Wait
  delay(500);
}
