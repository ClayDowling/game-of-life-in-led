//
// Created by clay on 12/27/2019.
//

#include "rules.h"

bool isAlive(uint8_t neighbors, bool alive) {
  if (2 == neighbors) {
    return alive;
  }
  if (3 == neighbors)
    return true;
  return false;
}
