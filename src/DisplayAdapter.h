#ifndef _DISPLAYADAPTER_H_
#define _DISPLAYADAPTER_H_

#include <stdint.h>

class DisplayAdapter {

public:
  virtual void begin(void) = 0;
  virtual void drawPixel(int16_t x, int16_t y, int16_t c) = 0;
  virtual uint16_t color(uint8_t r, uint8_t g, uint8_t b) = 0;
};

#endif