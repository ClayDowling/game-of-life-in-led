#include "LedDisplayAdapter.h"

LedDisplayAdapter::LedDisplayAdapter(uint8_t a, uint8_t b, uint8_t c, uint8_t d,
                                     uint8_t clk, uint8_t lat, uint8_t oe,
                                     boolean dbuf, uint8_t width = 32)
    : p(a, b, c, d, clk, lat, oe, dbuf, width) {}

void LedDisplayAdapter::begin(void) { p.begin(); }
void LedDisplayAdapter::drawPixel(int16_t x, int16_t y, int16_t c) {
  p.drawPixel(x, y, c);
}
uint16_t LedDisplayAdapter::color(uint8_t r, uint8_t g, uint8_t b) {
  return p.Color333(r, g, b);
}