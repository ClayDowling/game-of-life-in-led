#include "DisplayAdapter.h"
#include "RGBmatrixPanel.h"

class LedDisplayAdapter : public DisplayAdapter {
public:
  LedDisplayAdapter(uint8_t a, uint8_t b, uint8_t c, uint8_t d, uint8_t clk,
                    uint8_t lat, uint8_t oe, boolean dbuf, uint8_t width = 32);

  virtual void begin(void) override;
  virtual void drawPixel(int16_t x, int16_t y, int16_t c) override;
  virtual uint16_t color(uint8_t r, uint8_t g, uint8_t b) override;

private:
  RGBmatrixPanel p;
};