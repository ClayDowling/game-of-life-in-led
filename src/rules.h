//
// Created by clay on 12/27/2019.
//

#ifndef CONWAY_RULES_H
#define CONWAY_RULES_H

#include <stdint.h>

bool isAlive(uint8_t neighbors, bool alive);

#endif // CONWAY_RULES_H
