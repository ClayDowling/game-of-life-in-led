#ifndef _GAME_H_
#define _GAME_H_

#include "DisplayAdapter.h"
#include "board.h"
#include <Arduino.h>

extern DisplayAdapter *matrix;
extern Board *currentGeneration;
extern Board *futureGeneration;

void gameSetup(void);
void gameLoop(void);

#endif