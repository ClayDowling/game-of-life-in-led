//
// Created by clay on 12/27/2019.
//
#include "board.h"
#include <stdlib.h>

void Board::set(uint8_t x, uint8_t y, bool state) { value[x][y] = state; }

bool Board::get(uint8_t x, uint8_t y) {
  if (x >= XMAX || y >= YMAX)
    return false;
  return value[x][y];
}

uint8_t Board::neighbors(uint8_t x, uint8_t y) {
  uint8_t count = 0;
  uint8_t xlb = x == 0 ? 0 : x - 1;
  uint8_t xub = x >= XMAX - 1 ? XMAX - 1 : x + 1;
  uint8_t ylb = y == 0 ? 0 : y - 1;
  uint8_t yub = y >= YMAX - 1 ? YMAX - 1 : y + 1;

  for (uint8_t i = xlb; i <= xub; ++i) {
    for (uint8_t j = ylb; j <= yub; ++j) {
      if (x == i && y == j) {
        continue;
      }
      if (get(i, j)) {
        ++count;
      }
    }
  }
  return count;
}