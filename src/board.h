//
// Created by clay on 12/27/2019.
//

#ifndef CONWAY_BOARD_H
#define CONWAY_BOARD_H

#include <stdint.h>

#define XMAX 64
#define YMAX 32

class Board {
protected:
  bool value[XMAX][YMAX];

public:
  void set(uint8_t x, uint8_t y, bool state);
  bool get(uint8_t x, uint8_t y);
  uint8_t neighbors(uint8_t x, uint8_t y);
};

#endif // CONWAY_BOARD_H
